package DataManager;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONStringer;

public class TraTableJsonSevlet extends HttpServlet
{

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		
		
		response.setHeader("Access-Control-Allow-Origin", "*");
		response.setContentType("text/javascript;charset=UTF-8");
		PrintWriter out = response.getWriter();
		//out.println("<p>hhh</p>");
		try
		{
			javax.naming.Context ctx = new javax.naming.InitialContext();
			// 根据webdb数据源获得DataSource对象
			javax.sql.DataSource ds = (javax.sql.DataSource) ctx
					.lookup("java:/comp/env/jdbc/trajectory");
			Connection conn = ds.getConnection();
		    
			
			// 执行SQL语句
			PreparedStatement pstmt = conn
					.prepareStatement("SELECT T_TName, T_TNumber,T_TOwner FROM trajectory.t_targetinfo where T_TName !='none' ");
			ResultSet rs = pstmt.executeQuery();
			
			
			 JSONStringer stringer = new JSONStringer();  
			   JSONArray tableHead = new JSONArray("['车牌号','车主','时间']");
			    JSONStringer tableStr = new JSONStringer(); 
			    JSONArray   tableSet = new JSONArray();
			    int tID;  
			    String utcTime, carNum, carOwn,carTime,speed,head,asy;  
				
				 try {  
					 
				        while (rs.next()) // 生成查询结果
						{
				        	
				        	carNum = rs.getString("T_TNumber");  
				        	carOwn = rs.getString("T_TOwner"); 
				        	carTime = rs.getString("T_TName");	
				        	
				        	if(carOwn.length()>1)
				        	{
				        		char [] str = carOwn.toCharArray();
				        		str[1] = '*';
				        		carOwn = String.valueOf(str);
				        	}
				        	
				        	JSONStringer row = new JSONStringer();
				        	row.object().key("Num").value(carNum)
				        		.key("Own").value(carOwn)
				        		.key("Time").value(carTime).endObject();
				        					            
				        	tableSet.put(row);  
						}
				        tableStr.object().key("tableHead").value(tableHead)
				        		.key("tableData").value(tableSet).endObject();
				        		        
				        
				   } catch (JSONException e) {  
				        e.printStackTrace();  
				    } catch (Exception e) {  
				        e.printStackTrace();  
				    }  
				 out.println(tableStr.toString());

			
			
			
			//String result = request.getParameter("callback") + "(" + table.toString() + ")";			    
			//response.setContentType("text/javascript");
			//response.getWriter().print(result);
			
			pstmt.close();  // 关闭PreparedStatement对象
			
		}
		catch (Exception e)
		{
			out.println(e.getMessage());
		}
	
	}

}
