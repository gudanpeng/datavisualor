package DataManager;

import java.awt.geom.Point2D;
import java.beans.FeatureDescriptor;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.channels.SeekableByteChannel;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.catalina.Session;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import AnalyticsManager.ClusterDBSCAN;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.CoordinateList;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.io.ParseException;
import com.vividsolutions.jts.io.WKBReader;

import JMPL.*;

public class ClusterServlet extends HttpServlet {
	
	public class DBSCANTask implements Callable<JSONArray> {
		
		private Connection _conn;
		private String _collectName;
		private String _datetime;
		private JSONArray _ids;
		private double _esp;
		private int _minPnt;
		private HttpSession _session;
		
		public DBSCANTask(Connection conn,
				String collectName,
				String currentDatetime,
				JSONArray ids,
				double esp,
				int minPnt,
				HttpSession session) {
			super();
			_conn = conn;
			_collectName = collectName;
			_datetime = currentDatetime;
			_ids = ids;
			_esp = esp;
			_minPnt = minPnt;
			_session = session;
		}
		
		@Override
		public JSONArray call() throws Exception {
			// TODO Auto-generated method stub
			return calculateTrackWithSession(_conn,_collectName,_datetime,_ids,_esp,_minPnt,_session);
		}
		
	}
	
	public Projection projection;
	public Ellipsoid ellipsoid;
	/**
	 * Constructor of the object.
	 */
	public ClusterServlet() {
		super();
		projection = new MercatorProjection();
		ellipsoid = new Ellipsoid("", 6378137 , 6378137 , 0, ""); // Sphere  EPSG:3857
		projection.setEllipsoid(ellipsoid);
		projection.initialize();
	}

	/**
	 * Destruction of the servlet. <br>
	 */
	public void destroy() {
		super.destroy(); // Just puts "destroy" string in log
		// Put your code here
	}

	/**
	 * The doGet method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {


		response.setHeader("Access-Control-Allow-Origin", "*");
		response.setContentType("text/json;charset=UTF-8");
		
		
		//  Type Multipoint MultiLineString0.
		
		
		//URL ： ~ClusterServlet? TID= &TOwner= &TNumber= &TStartTime= &TEndTime= &Type= &DistanceThreshold=&tDistanceThreshold=

	     //获取数据库数据
				 
	}

	/**
	 * The doPost method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to post.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setHeader("Access-Control-Allow-Origin", "*");
		response.setHeader("Access-Control-Allow-Headers", "Content-Type");
		response.setHeader("Access-Control-Allow-Methods","PUT,POST,GET,DELETE,OPTIONS");
		response.setContentType("text/json;charset=UTF-8");
		
		//  Type Multipoint MultiLineString0.
		
		//URL ： ~ClusterServlet? TID= &TOwner= &TNumber= &TStartTime= &TEndTime= &Type= &DistanceThreshold=&tDistanceThreshold=

	     //获取数据库数据
	     try
			{
	    	 
		    	    	 		     
				 
				 // 获取body
			     BufferedReader br = new BufferedReader(new InputStreamReader((ServletInputStream) request.getInputStream(), "utf-8"));
			     StringBuffer sb = new StringBuffer("");
			     String temp;
			     while ((temp = br.readLine()) != null) { 
			       sb.append(temp);
			     }
			     br.close();
			     String jsonStr = sb.toString();
			     
			     JSONObject body = new JSONObject(jsonStr);
			     
//			     POST Body
//			     {
//			    	    collectionName: _collectionsName_,
//			    	    datetime:_datetime_,
//			    	    fields: _fields_,
//			     		id: _ids_,
//			    	    parameter: {
//			    	        distance: _dis_,
//			    	        minTrs : _minTrs_
//			    	    }
//			     }
			     String collectionName = body.getString("collectionName");
			     JSONArray datetime =  body.getJSONArray("datetime");
			     JSONArray ids = body.getJSONArray("id");
			     double distance = body.getJSONObject("parameter").getDouble("distance");
			     int minTrs = body.getJSONObject("parameter").getInt("minTrs");
			     
			     
			     // create Sesssion
			     HttpSession session = request.getSession();
			     if (session.isNew()) {
			    	session.setAttribute("name", "User1");
			    	session.setAttribute("lastBody", body);
				} else {
					JSONObject lastBodyJsonObject = (JSONObject) session.getAttribute("lastBody");
					String lastCollectionName = lastBodyJsonObject.getString("collectionName");
					JSONArray lastDatetime = lastBodyJsonObject.getJSONArray("datetime");
				    JSONArray lastIds = lastBodyJsonObject.getJSONArray("id");
				    double lastDistance = lastBodyJsonObject.getJSONObject("parameter").getDouble("distance");
				    int lastMinTrs = lastBodyJsonObject.getJSONObject("parameter").getInt("minTrs");
				    
				    if (lastCollectionName.equals(collectionName) && 
				    		lastIds.toString().equals(ids.toString()) &&
				    		lastDatetime.toString().equals(datetime.toString())) {
				    	// 根据以获取的 数据集，重新计算聚类
				    	JSONArray data = new JSONArray();
						for (int i = 0; i < datetime.length(); i++) {
							String currentDatetime = datetime.getString(i).replace("-", "");
							
							 //ClusterDBSCAN dbscan = (ClusterDBSCAN) session.getAttribute("clusterDBSCAN");
					    	Map<String, ClusterDBSCAN> storeClusterDB = (Map<String, ClusterDBSCAN>) session.getAttribute("ClusterObjects" + currentDatetime);
					        Map<String, List<Coordinate[]>> storeLines = (Map<String, List<Coordinate[]>>) session.getAttribute("ObjectsLines" + currentDatetime);
					    	//
					        JSONArray dataItem = getDataFromStoreDBSCAN(currentDatetime, ids, storeClusterDB, storeLines, distance, minTrs);
					        if (dataItem.length() == 1) data.put(dataItem.get(0));
						}
				        
				        JSONObject reponseJsonObject = new JSONObject();
				        reponseJsonObject.put("datetime", datetime).put("timeunit", "1hh").put("id", ids).put("data", data);
					    
					    response.getOutputStream().write(reponseJsonObject.toString().getBytes("UTF-8"));  
					    return ;
				    }
				}
			     
			    // 计算新的 返回数据		    	
		    	JSONArray data = null;
		    	
			    DBConn.Init();	
				Connection conn = DBConn.getConn();		
				 
			    if ( collectionName.equalsIgnoreCase("shbasestation")) {
			    	data = getBaseStationDBSCAN(conn,distance,minTrs);
			    } else {
//				    	data = calculateTrack(conn,ids,distance,minTrs);
//					    data = calculateTrackPoints(conn,ids,distance,minTrs);
			    	data = calculateTrackWithSession(conn,collectionName,datetime,ids,distance,minTrs,session);
			    }
			     
			     
			    JSONObject reponseJsonObject = new JSONObject();
			    reponseJsonObject.put("datetime", datetime).put("timeunit", "1hh").put("id", ids).put("data", data);
			    
			    response.getOutputStream().write(reponseJsonObject.toString().getBytes("UTF-8"));  
			    DBConn.Recycle(conn);
			    
				
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
			} 
		
	}
	
	/**
	 * The doPost method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to post.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doOptions(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setHeader("Access-Control-Allow-Origin", "*");
		response.setHeader("Access-Control-Allow-Headers", "Content-Type");
		response.setHeader("Access-Control-Allow-Methods","PUT,POST,GET,DELETE,OPTIONS");
		response.setContentType("text/json;charset=UTF-8");
	}
	
	public void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, java.io.IOException {
		resp.setHeader("Access-Control-Allow-Origin", "*");
		resp.setHeader("Access-Control-Allow-Headers", "Content-Type");
		resp.setHeader("Access-Control-Allow-Methods","PUT,POST,GET,DELETE,OPTIONS");
		resp.setContentType("text/json;charset=UTF-8");
		
		String method = req.getMethod();
        if(method.equals("GET")) {
        	doGet(req, resp);
        } else if (method.equals("HEAD")) {            
            doHead(req, resp);
        } else if (method.equals("POST")) {
            doPost(req, resp);
        } else if (method.equals("PUT")) {
            doPut(req, resp);
		} else if (method.equals("DELETE")) {
            doDelete(req, resp);
		} else if (method.equals("OPTIONS")) {
            doOptions(req, resp);
		} else if (method.equals("TRACE")) {
            doTrace(req, resp);
        } else {
            resp.sendError(501, "something error");
        }
	}

	/**
	 * Initialization of the servlet. <br>
	 *
	 * @throws ServletException if an error occurs
	 */
	public void init() throws ServletException {
		// Put your code here
	}
	
	// 获取轨迹点 DBSCAN
	public JSONArray calculateTrackPoints(Connection conn, JSONArray ids, double esp, int minPnt) throws SQLException, JSONException {
		String queryString = " select a.\"Date\",a.\"IMEI\", " 
				+ " ST_AsBinary( "
				+ " 		ST_Transform( "
				+ "				ST_Force_2d("
				+ "					ST_GeomFromWKB(ST_AsBinary(a.userPath),4326)"
				+ "				), "
				+ " 			3857 "
				+ " 		) "
				+ " 	) "
				+ "  as geom "
				+ " from shcellphonetrack_geo as a, ( "
				+ " select \"IMEI\" from shcellUserCluster where clusterid10 = ?)t "
				+ " where a.\"IMEI\" = t.\"IMEI\" and a.\"Date\" = '20160301'";
		 
		PreparedStatement stmt = conn.prepareStatement(queryString);
	    
		JSONArray clusterid = new JSONArray();
		JSONArray trajectories = new JSONArray();
		JSONArray data = new JSONArray();
	    for(int i = 0; i<ids.length(); i++ ){
	    	int id = ids.getInt(i);
	    	stmt.setLong(1, id);
   		 	ResultSet rs = stmt.executeQuery();
   		 	// 执行SQL语句
   		 	
   		 	// 当前集合的所有轨迹 点集合
   		 	CoordinateList coordsList = new CoordinateList();
   		 	// 获取每个集合 的 所有移动对象的轨迹数据
	   		while (rs.next()) // 生成查询结果
	 		{
	 	    	String date = rs.getString("Date");
	 	    	byte[] geom = rs.getBytes("geom");
	 	    	WKBReader wkbReader = new WKBReader();
	 	    	try {
					Geometry linesTrack = wkbReader.read(geom);
					Coordinate [] coords = linesTrack.getCoordinates();						
					coordsList.add(coords, false);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	 		}
	   		
	   		//原始 点集合
//	   		clusterid.put(id);
//	   		Coordinate[] iteratorCoords = coordsList.toCoordinateArray();
//	   		toWGS84(iteratorCoords);
//	   		
//   			StringBuffer coords_buffer = new StringBuffer("");
//   			
//   			if (iteratorCoords.length > 0) coords_buffer.append(iteratorCoords[0].toString());
//   			
//   			for (int c_index = 1; c_index < iteratorCoords.length; c_index++) {
//   				coords_buffer.append(",").append(iteratorCoords[c_index].toString());
//   			}
//		   	String coords = "[" + coords_buffer.toString() +"]";
//		   	coords = coords.replaceAll("\\(", "[");
//		   	coords = coords.replaceAll("\\)", "]");
//			trajectories.put("{\"coordinates\":" + coords + "}");
  
	   		
	   		// DBSCAN聚类 点集合
	   		List<Coordinate[]> coordsCluster = ClusterDBSCAN.dbscanPoints(coordsList, esp, minPnt);
			
	   		// 转换 轨迹线
	   		Iterator<Coordinate[]> iteratorCoordsIterator = coordsCluster.iterator();
	   		int clusterNum = 1;
	   		while(iteratorCoordsIterator.hasNext()) {
	   			Coordinate [] points = iteratorCoordsIterator.next();
	   			
	   			toWGS84(points);
	   			
	   			clusterid.put(clusterNum++);
	   			
	   			StringBuffer coords_buffer = new StringBuffer("");
	   			
	   			if (points.length > 0) coords_buffer.append(points[0].toString());
	   			
	   			for (int c_index = 1; c_index < points.length; c_index++) {
	   				coords_buffer.append(",").append(points[c_index].toString());
	   			}
			   	String coords = "[" + coords_buffer.toString() +"]";
			   	coords = coords.replaceAll("\\(", "[");
			   	coords = coords.replaceAll("\\)", "]");
				trajectories.put("{\"coordinates\":" + coords + "}");
				
	   		}
		}
	    JSONObject object = new JSONObject();
	    object.put("_id", "20160301").put("clusterid",clusterid).put("trajectories", trajectories);
	    
	    data.put(object);
	    
	    return data;
	}
	
	// 简化轨迹线
	public JSONArray calculateTrack(Connection conn, JSONArray ids, double esp, int minPnt) throws SQLException, JSONException {
		String queryString = " select a.\"Date\",a.\"IMEI\", " 
				+ " ST_AsBinary( "
				+ " 		ST_Transform( "
				+ "				ST_Force_2d("
				+ "					ST_GeomFromWKB(ST_AsBinary(a.userPath),4326)"
				+ "				), "
				+ " 			3857 "
				+ " 		) "
				+ " 	) "
				+ "  as geom "
				+ " from shcellphonetrack_geo as a, ( "
				+ " select \"IMEI\" from shcellUserCluster where clusterid10 = ?)t "
				+ " where a.\"IMEI\" = t.\"IMEI\" and a.\"Date\" = '20160301'";
		 
		PreparedStatement stmt = conn.prepareStatement(queryString);
	    
		JSONArray clusterid = new JSONArray();
		JSONArray trajectories = new JSONArray();
		JSONArray data = new JSONArray();
	    for(int i = 0; i<ids.length(); i++ ){
	    	int id = ids.getInt(i);
	    	stmt.setLong(1, id);
   		 	ResultSet rs = stmt.executeQuery();
   		 	// 执行SQL语句
   		 	
   		 	// 当前集合的所有轨迹 点集合
   		 	List<Coordinate[]> coordsLines = new LinkedList<Coordinate []>();
   		 	CoordinateList coordsList = new CoordinateList();
   		 	// 获取每个集合 的 所有移动对象的轨迹数据
	   		while (rs.next()) // 生成查询结果
	 		{
	 	    	String date = rs.getString("Date");
	 	    	byte[] geom = rs.getBytes("geom");
	 	    	WKBReader wkbReader = new WKBReader();
	 	    	try {
					Geometry linesTrack = wkbReader.read(geom);
					Coordinate [] coords = linesTrack.getCoordinates();
					coordsLines.add(coords);								
					coordsList.add(coords, false);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	 		}
	   		
	   		// DBSCAN聚类 点集合
	   		Coordinate[] coordsCentrCluster = ClusterDBSCAN.dbscanCentriodPoints(coordsList, esp, minPnt);
			
	   		// 转换 轨迹线
	   		Iterator<Coordinate[]> iteratorCoordsIterator = coordsLines.iterator(); 
	   		while(iteratorCoordsIterator.hasNext()) {
	   			Coordinate[] lines = iteratorCoordsIterator.next();
	   			Coordinate[] geoLines = changeTrackPoints(lines, coordsCentrCluster);
	   			clusterid.put(id);
	   			
	   			StringBuffer coords_buffer = new StringBuffer("");
	   			
	   			if (geoLines.length > 0) coords_buffer.append(geoLines[0].toString());
	   			
	   			for (int c_index = 1; c_index < geoLines.length; c_index++) {
	   				coords_buffer.append(",").append(geoLines[c_index].toString());
	   			}
			   	String coords = "[" + coords_buffer.toString() +"]";
			   	coords = coords.replaceAll("\\(", "[");
			   	coords = coords.replaceAll("\\)", "]");
				trajectories.put("{\"coordinates\":" + coords + "}");
	   		}	    
		}
	    JSONObject object = new JSONObject();
	    object.put("_id", "20160301").put("clusterid",clusterid).put("trajectories", trajectories);
	    
	    data.put(object);
	    
	    return data;
	}
	
	
	/**
	 * 获取 每个时间 点的 数据集合
	 * @param conn
	 * @param collectName
	 * @param datetime
	 * @param ids
	 * @param esp
	 * @param minPnt
	 * @param session
	 * @return
	 * @throws SQLException
	 * @throws JSONException
	 * @throws ExecutionException 
	 * @throws InterruptedException 
	 */
	public JSONArray calculateTrackWithSession(Connection conn,
			String collectName,
			JSONArray datetime,
			JSONArray ids,
			double esp,
			int minPnt,
			HttpSession session) throws SQLException, JSONException, InterruptedException, ExecutionException {
		
		ExecutorService exec = Executors.newCachedThreadPool();
		
		// 获取 所有时间粒度 下的 数据
		JSONArray data = new JSONArray();
		ArrayList<Future<JSONArray>> result = new ArrayList<Future<JSONArray>>();
		for (int i = 0; i < datetime.length(); i++) {
			String currentDatetime = datetime.getString(i).replace("-", "");
//			JSONArray dataItem = calculateTrackWithSession(conn,collectName,currentDatetime,ids,esp,minPnt,session);
			
			result.add( exec.submit(new DBSCANTask(conn,collectName,currentDatetime,ids,esp,minPnt,session)) );
			
//			if (dataItem.length() == 1) data.put(dataItem.get(0));
		}
		
		for (Future<JSONArray> fs:result) {
			JSONArray dataItem = fs.get();
			if (dataItem.length() == 1) data.put(dataItem.get(0));
		}
		
		return data;
	}
	
	
	// 简化轨迹线,并保存数据 以及 索引结构
	public JSONArray calculateTrackWithSession(Connection conn,
			String collectName,
			String datetime,
			JSONArray ids,
			double esp,
			int minPnt,
			HttpSession session) throws SQLException, JSONException {
		
		String datecondition = "a.\"Date\" = '" + datetime + "'";
		
		String queryString = " select a.\"Date\",a.\"IMEI\", " 
				+ " ST_AsBinary( "
				+ " 		ST_Transform( "
				+ "				ST_Force_2d("
				+ "					ST_GeomFromWKB(ST_AsBinary(a.userPath),4326)"
				+ "				), "
				+ " 			3857 "
				+ " 		) "
				+ " 	) "
				+ "  as geom "
				+ " from shcellphonetrack_geo as a, ( "
				+ " select \"IMEI\" from shcellUserCluster where clusterid10 = ?)t "
				+ " where a.\"IMEI\" = t.\"IMEI\" and "
				+ datecondition;
		 
		PreparedStatement stmt = conn.prepareStatement(queryString);
	    
		// session Map
		Map<String, ClusterDBSCAN> storeClusterDB = new HashMap<String, ClusterDBSCAN>();
		Map<String, List<Coordinate[]>> storeLines = new HashMap<String, List<Coordinate[]>>();
		
		JSONArray clusterid = new JSONArray();
		JSONArray trajectories = new JSONArray();
		JSONArray data = new JSONArray();
	    for(int i = 0; i<ids.length(); i++ ){
	    	int id = ids.getInt(i);
	    	stmt.setLong(1, id);
   		 	ResultSet rs = stmt.executeQuery();
   		 	// 执行SQL语句
   		 	
   		 	// 当前集合的所有轨迹 点集合
   		 	List<Coordinate[]> coordsLines = new LinkedList<Coordinate []>();
   		 	CoordinateList coordsList = new CoordinateList();
   		 	// 获取每个集合 的 所有移动对象的轨迹数据
	   		while (rs.next()) // 生成查询结果
	 		{
	 	    	String date = rs.getString("Date");
	 	    	byte[] geom = rs.getBytes("geom");
	 	    	WKBReader wkbReader = new WKBReader();
	 	    	try {
					Geometry linesTrack = wkbReader.read(geom);
					Coordinate [] coords = linesTrack.getCoordinates();
					coordsLines.add(coords);								
					coordsList.add(coords, false);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	 		}
	   		
	   		// DBSCAN聚类 点集合
	   		ClusterDBSCAN dbscan = new ClusterDBSCAN(coordsList, esp, minPnt);
	   		Coordinate[] coordsCentrCluster = dbscan.getDBSCANCentriodPoints();
	   		
	   		storeClusterDB.put(String.valueOf(id), dbscan);
	   		storeLines.put(String.valueOf(id), coordsLines);
			
	   		// 转换 轨迹线
//	   		Iterator<Coordinate[]> iteratorCoordsIterator = coordsLines.iterator(); 
//	   		while(iteratorCoordsIterator.hasNext()) {
//	   			Coordinate [] lines = iteratorCoordsIterator.next();
//	   			Coordinate[] geoLines = changeTrackPoints(lines, coordsCentrCluster);
//	   			clusterid.put(id);
//	   			
//	   			StringBuffer coords_buffer = new StringBuffer("");
//	   			
//	   			if (geoLines.length > 0) coords_buffer.append(geoLines[0].toString());
//	   			
//	   			for (int c_index = 1; c_index < geoLines.length; c_index++) {
//	   				coords_buffer.append(",").append(geoLines[c_index].toString());
//	   			}
//			   	String coords = "[" + coords_buffer.toString() +"]";
//			   	coords = coords.replaceAll("\\(", "[");
//			   	coords = coords.replaceAll("\\)", "]");
//				trajectories.put("{\"coordinates\":" + coords + "}");
//	   		}
	   		
	   		// 将相似的轨迹 统计 返回统计会的轨迹
	   		Map<String, Integer> numLines = new HashMap<String, Integer>();
	   		Iterator<Coordinate[]> iteratorCoordsIterator = coordsLines.iterator(); 
	   		while(iteratorCoordsIterator.hasNext()) {
	   			Coordinate [] lines = iteratorCoordsIterator.next();
	   			Coordinate[] geoLines = changeTrackPoints(lines, coordsCentrCluster);
	   			
	   			// create linestring
	   			StringBuffer coords_buffer = new StringBuffer("");
	   			
	   			if (geoLines.length > 0) coords_buffer.append(geoLines[0].toString());
	   			
	   			for (int c_index = 1; c_index < geoLines.length; c_index++) {
	   				coords_buffer.append(",").append(geoLines[c_index].toString());
	   			}
	   			
	   			String currentLineString = coords_buffer.toString();
	   			
	   			// add Linestring to Map
	   			if ( numLines.containsKey(currentLineString)) {
	   				int count = numLines.get(currentLineString) + 1;
	   				
	   				numLines.put(currentLineString, count);
	   				
	   			} else {
	   				
	   				numLines.put(currentLineString, 1);
	   				
	   			}
	   		}
	   		

	   		Iterator<Map.Entry<String, Integer>> linesIteratorr = numLines.entrySet().iterator();
		   	while (linesIteratorr.hasNext()) {
		   		Map.Entry<String, Integer> entry = (Map.Entry<String, Integer>) linesIteratorr.next();
		   		String lineString = (String) entry.getKey();
		   		int count = (Integer) entry.getValue();
		   	
		   		clusterid.put(id);
	   			
			   	lineString = "[" + lineString +"]";
	   			lineString = lineString.replaceAll("\\(", "[");
	   			lineString = lineString.replaceAll("\\)", "]");
				trajectories.put("{\"coordinates\":" + lineString + "," 
						+ "\"count\":"+ count +"}");	
		   	}
	   		
		}
	    JSONObject object = new JSONObject();
	    object.put("_id", datetime).put("clusterid",clusterid).put("trajectories", trajectories);
	    
	    data.put(object);
	    
	    // add Session
	    session.setAttribute("ClusterObjects" + datetime, storeClusterDB);
	    session.setAttribute("ObjectsLines"  + datetime, storeLines);
	    
	    return data;
	}
	
	// 根据 Session 构建轨迹
	public JSONArray getDataFromStoreDBSCAN(
			String datetime,
			JSONArray ids,
			Map<String, ClusterDBSCAN> storeClusterDB,
			Map<String, List<Coordinate[]>> storeLines,
			double esp,
			int minPnt) throws JSONException {
		// 转换 轨迹线
		JSONArray clusterid = new JSONArray();
		JSONArray trajectories = new JSONArray();
		JSONArray data = new JSONArray();
		for(int i = 0; i<ids.length(); i++ ){
		   	int id = ids.getInt(i); 
		   	
		   	ClusterDBSCAN dbscan = storeClusterDB.get(String.valueOf(id));
		   	List<Coordinate[]> linesCluster = storeLines.get(String.valueOf(id));
		   	
		   	Coordinate[] coordsCentrCluster = dbscan.getDBSCANCentriodPoints(esp, minPnt);
		   	
		   	Iterator<Coordinate[]> iteratorC = linesCluster.iterator();
		   	while(iteratorC.hasNext()) {
		   		Coordinate[] lines =   iteratorC.next();
   	   			Coordinate[] geoLines = changeTrackPoints(lines, coordsCentrCluster);
   	   			clusterid.put(id);
   	   			
   	   			StringBuffer coords_buffer = new StringBuffer("");
   	   			
   	   			if (geoLines.length > 0) coords_buffer.append(geoLines[0].toString());
   	   			
   	   			for (int c_index = 1; c_index < geoLines.length; c_index++) {
   	   				coords_buffer.append(",").append(geoLines[c_index].toString());
   	   			}
   			   	String coords = "[" + coords_buffer.toString() +"]";
   			   	coords = coords.replaceAll("\\(", "[");
   			   	coords = coords.replaceAll("\\)", "]");
   				trajectories.put("{\"coordinates\":" + coords + "}");
		   	}
   			
		}
		
		JSONObject object = new JSONObject();
		object.put("_id", datetime).put("clusterid",clusterid).put("trajectories", trajectories);
		    
		data.put(object);
		
		return data;
	}
	
	// 更改 轨迹点
	public Coordinate[] changeTrackPoints(Coordinate[] lines, Coordinate[] dbscan) {
		Coordinate[] resultCoordinates  = new Coordinate[lines.length];
		for (int i = 0; i < lines.length; i++) {
			Coordinate c = lines[i];
			Coordinate pos = new Coordinate();
			double nearest = Double.MAX_VALUE;
			for (Coordinate d:dbscan) {
				double dis = c.distance(d);
				if (dis < nearest) {
					nearest = dis;
					pos.x = d.x;
					pos.y = d.y;
				}
			}
			resultCoordinates[i] = toWGS84(pos);
		}
		return resultCoordinates;
	}
	
	// 对相似的 轨迹点进行统计
	
	
	//  更改投影坐标
	public Coordinate toWGS84 (Coordinate point) {
		Coordinate pCoordinate = new Coordinate();
		if (projection.hasInverse()) {
			Point2D.Double mapP = new Point2D.Double(0,0);
			Point2D.Double geoP = new Point2D.Double(0,0);
			mapP.x = point.x;
			mapP.y = point.y;
			projection.projectInverse(mapP, geoP);
			pCoordinate.x = geoP.x;
			pCoordinate.y = geoP.y;
			pCoordinate.z = 0;
		}
		return pCoordinate;
	}
	
	public void toWGS84(Coordinate[] points) {
		
		for(int i = 0; i < points.length; i++) {
			points[i] = toWGS84(points[i]);
		}
		
	}

	// 对基站进行 DBSCAN
	public JSONArray getBaseStationDBSCAN(Connection conn, double esp, int minPnt) throws SQLException, JSONException {
	//  查询 DBSCAN 聚类集合
	     String queryStr = "select cid,array_agg(lon || ',' || lat) as coords from ( "
	    		 + "select lon,lat,ST_ClusterDBSCAN( "
	    		 + " ST_Transform(ST_GeomFromEWKT('SRID=4326;' || ST_AsText(ST_MakePoint(lon,lat))),3857), "
	    		 + esp + ", "
	    		 + minPnt
	    		 + " ) over () AS cid "
	    		 + " from shbasestation)t "
	    		 + " where cid IS NOT NULL "
	    		 + " group by cid order by cid ASC ";
	     
	     // 查询 DBSCAN 聚类集合，返回 每个集合的 中心点 集合
	     String queryCenterString = " select cid, ST_X(coords) as lonCenter,  ST_Y(coords) as latCenter from ( "
	    		 + " select cid, "
	    		 + " ST_Transform( "
	    		 + " ST_Centroid( "
	    		 + "		ST_Transform(ST_GeomFromEWKT('SRID=4326;MULTIPOINT(' || string_agg('(' || lon || ' ' || lat || ')', ',') || ')'),3857) " 
	    		 + "		), "
	    		 + "		4326 "
	    		 + "	) as coords " 
	    		 + " from ( "
	    		 + " select lon,lat,ST_ClusterDBSCAN( "
	    		 + "	ST_Transform(ST_GeomFromEWKT('SRID=4326;' || ST_AsText(ST_MakePoint(lon,lat))),3857), "
	    		 + esp + ","
	    		 + minPnt
	    		 + " ) over () AS cid "
	    		 + " from shbasestation "
	    		 + " )t  "
	    		 + " where cid IS NOT NULL "
	    		 + " group by cid order by cid ASC)a";
	     
	     PreparedStatement stmt = conn.prepareStatement(queryCenterString);
	     
	     ResultSet rs = stmt.executeQuery();
	     
	     
	     JSONArray clusterid = new JSONArray();
	     JSONArray trajectories = new JSONArray();
	     while (rs.next()) // 生成查询结果
		 {
	    	 int cid = Integer.valueOf(rs.getString("cid"));
//	         String coords = rs.getString("coords");
//	         String coords_Array = coords.substring(1, coords.length() - 1);
//	         
//	         coords = "[" + coords_Array + "]";
//	         coords = coords.replaceAll("\\\"(([0-9]|\\.)+,([0-9]|\\.)+)\\\"", "[$1]");
	         
			 clusterid.put(cid);
//			 trajectories.put("{\"coordinates\":" + coords + "}");
			 
			 double lon = rs.getDouble(2);
			 double lat = rs.getDouble(3);
			 String coords = "[[" + lon + "," + lat + "]]";
			 trajectories.put("{\"coordinates\":" + coords + "}");
	        	
		 }
	     JSONObject object = new JSONObject();
	     object.put("_id", "20160301").put("clusterid",clusterid).put("trajectories", trajectories);
	     JSONArray data = new JSONArray();
	     data.put(object);
	     
	     return data;
	}
}
