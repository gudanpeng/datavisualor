package DataManager;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class TraTableSevlet extends HttpServlet
{

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		
		
		response.setHeader("Access-Control-Allow-Origin", "*");
		response.setContentType("text/javascript;charset=UTF-8");
		PrintWriter out = response.getWriter();
		//out.println("<p>hhh</p>");
		try
		{
			javax.naming.Context ctx = new javax.naming.InitialContext();
			// 根据webdb数据源获得DataSource对象
			javax.sql.DataSource ds = (javax.sql.DataSource) ctx
					.lookup("java:/comp/env/jdbc/trajectory");
			Connection conn = ds.getConnection();
		    
			
			// 执行SQL语句
			PreparedStatement pstmt = conn
					.prepareStatement("SELECT T_TName, T_TNumber,T_TOwner FROM trajectory.t_targetinfo where T_TName !='none' ");
			ResultSet rs = pstmt.executeQuery();
			StringBuilder table = new StringBuilder();
			table.append("<table border='1'>");
			table.append("<tr><td>车牌号</td><td>车主</td><td>时间</td></tr>");
			while (rs.next()) // 生成查询结果
			{
				table.append("<tr><td>" + rs.getString("T_TNumber") + "</td><td>");
				table.append(rs.getString("T_TOwner") + "</td><td>");
				table.append(rs.getString("T_TName") + "</td></tr>");
				
			}
			table.append("</table>");
			out.println(table.toString()); // 输出查询结果
			
			//String result = request.getParameter("callback") + "(" + table.toString() + ")";			    
			//response.setContentType("text/javascript");
			//response.getWriter().print(result);
			
			pstmt.close();  // 关闭PreparedStatement对象
			
		}
		catch (Exception e)
		{
			out.println(e.getMessage());
		}
	
	}

}
