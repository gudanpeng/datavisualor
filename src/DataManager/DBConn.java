package DataManager;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;

import javax.naming.NamingException;

import org.apache.taglibs.standard.resources.Resources;



public class DBConn {
	public static class MyConnect
	{
		public 	Connection conn = null;
		public 	long	   useCount = 0;
	}; 
	
	static private ArrayList<MyConnect> ListConn = new ArrayList<MyConnect>();//连接集合

//	static private String strMaxConn = ResourceBundle.getBundle("jdbc").getString("maxConn");//读取配置的查询sql
	static private int maxConn = 20;//Integer.parseInt(strMaxConn);
		
	static public  void Init(){
		try {
			if(ListConn.isEmpty()){
				System.out.println("<<-----初始化连接池----->>");
				for(int i = 0;i<maxConn;i++){
					MyConnect myconn = new MyConnect();
					myconn.conn = DBConn.getConnection();
					myconn.useCount = 0;
					ListConn.add(myconn);
				}
				System.out.println("<<-----初始化连接池完成:共"+maxConn+"连接数----->>");
			}
		} catch (SQLException e){
			System.out.println("<<-----初始化连接池失败----->>");
			e.printStackTrace();
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
	private static Connection getConnection() throws NamingException, SQLException {
		// TODO Auto-generated method stub
		 javax.naming.Context ctx = new javax.naming.InitialContext();
		 javax.sql.DataSource ds = (javax.sql.DataSource) ctx.lookup("java:/comp/env/jdbc/trajectoryPost");
		 Connection conn = ds.getConnection();
				
		return conn;
	}


	static public synchronized Connection getConn() {
		int index  = -1;
		long count = 99999999;		
		for (int i = 0; i < ListConn.size(); i++)
		{	
			if (ListConn.get(i).useCount < count)
			{
				index = i;
				count = ListConn.get(i).useCount;
			}
		}
		//添加引用
		ListConn.get(index).useCount++;
		return ListConn.get(index).conn;
	}
	
	static public synchronized void Recycle(Connection conn)
	{
		for (int i = 0; i < ListConn.size(); i++)
		{
			if (conn.equals(ListConn.get(i).conn))
			{
				ListConn.get(i).useCount--;
				break;
			}
		}
		return ;
	}
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		DBConn.Init();		
		Connection c1 = DBConn.getConn();		
		Connection c2 = DBConn.getConn();
		for (int i = 0; i < ListConn.size(); i++)
		{
			String str = "第" + Integer.toString(i) + "个连接引用计数:" + Integer.toString((int)ListConn.get(i).useCount) + "\n";
			System.out.println(str);			
		}
		DBConn.Recycle(c1);
		DBConn.Recycle(c2);		
		for (int i = 0; i < ListConn.size(); i++)
		{
			String str = "第" + Integer.toString(i) + "个连接引用计数:" + Integer.toString((int)ListConn.get(i).useCount) + "\n";
			System.out.println(str);			
		}		
    }
}
