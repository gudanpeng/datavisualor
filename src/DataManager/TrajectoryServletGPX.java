package DataManager;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.alternativevision.gpx.beans.GPX;
import org.alternativevision.gpx.beans.Track;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;
import org.json.JSONWriter;

public class TrajectoryServletGPX extends HttpServlet {

	/**
	 * Constructor of the object.
	 */
	public TrajectoryServletGPX() {
		super();
	}

	/**
	 * Destruction of the servlet. <br>
	 */
	public void destroy() {
		super.destroy(); // Just puts "destroy" string in log
		// Put your code here
	}

	/**
	 * The doGet method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

			
//	     response.setContentType("text/plain;charset=utf-8");
//	     request.setCharacterEncoding("utf-8");
	     
//	     PrintWriter out = response.getWriter();
//	     String data = "[{name:\"胡阳\",age:24},{name:\"胡阳\",age:23}]";//构建的json数据
//	     out.println(data);
		response.setHeader("Access-Control-Allow-Origin", "*");
		response.setContentType("text/json;charset=UTF-8");
		
		
		//  Type Multipoint MultiLineString
		
		//URL ： ~ServletJson? TID= &TOwner= &TNumber= &TStartTime= &TEndTime= &Type=

	     //获取数据库数据
	     try
			{
				javax.naming.Context ctx = new javax.naming.InitialContext();
				// 根据webdb数据源获得DataSource对象
				javax.sql.DataSource ds = (javax.sql.DataSource) ctx
						.lookup("java:/comp/env/jdbc/trajectory");
				Connection conn = ds.getConnection();
				
							
//				 Map map = request.getParameterMap();  
//			     Set<String> keySet = map.keySet();  
//			     for (String key : keySet) {  
//			        String[] values = (String[]) map.get(key);  
//			        for (String value : values) {  
//			            System.out.println(key+"="+value);  
//			        }
//			     }
				
			     byte dID[] = request.getParameter("TID").getBytes("ISO-8859-1");
			     String tId = new String(dID, "utf-8");
				
			     byte dOwner[] = request.getParameter("TOwner").getBytes("ISO-8859-1");
			     String tOwner = new String(dOwner, "utf-8");
			     
			     byte carNum[] = request.getParameter("TNumber").getBytes("ISO-8859-1");
			     String carNumber = new String(carNum, "utf-8");
			     
			     byte startT[] = request.getParameter("TStartTime").getBytes("ISO-8859-1");
			     String startTime = new String(startT, "utf-8");
			     
			     byte endT[] = request.getParameter("TEndTime").getBytes("ISO-8859-1");
			     String endTime = new String(endT, "utf-8");
			     
			     byte typeF[] = request.getParameter("Type").getBytes("ISO-8859-1");
			     String typeFeatrue = new String(typeF, "utf-8");
			    
			     
			    // carNumber = "鄂AXW375";
			     
			     if(carNumber==null) return ;
			     if(startTime==null) return ;
			     if(endTime == null) return ;
			     if(typeFeatrue==null) typeFeatrue="MultiPoint";
			     
			     Calendar calendar = Calendar.getInstance();
			     long startDate = this.toGMT(startTime);
			     long endDate =  this.toGMT(endTime);
			     long interval =  endDate - startDate;
			     long dayNumber = interval /(24*60*60);
			     
			     calendar.setTime(new SimpleDateFormat("yyyy-MM-dd").parse(startTime));
			     			     
			     startTime = String.valueOf(startDate);
			     endTime = String.valueOf(endDate);
			     
			     
			    
			     List<String> tableSet = getTableSet(conn,startTime,endTime);
			     String CarID          = getCarID(conn,carNumber);
			     
			     		 
				   
				 JSONStringer geoStringer = new JSONStringer(); 
				 JSONArray   coordinatePoints = new JSONArray();
				 String queryCommd = "";
				 
				 
				 Iterator<String> it = tableSet.iterator();
				 
				 if(it.hasNext())
				 {
					 String tableName = it.next();
					 
					 
				     String queryFromTime = "AND T_UTCTime between  "
					     		+ startTime
					     		+ " and "
					     		+ endTime ;
				     
				    	     
				     String queryStr = " SELECT  T_UTCTime,T_Longitude,T_Latitude,T_Speed,T_Heading,T_AsynFlag "
				    		 	     + "FROM  trajectory."+ tableName
				                     + " where trajectory."+tableName+".T_TargetID = '"
				                     + CarID + "'  "
				                     + queryFromTime
							     	 + " order by T_UTCTime ASC"
							     	 + " limit 10 ";
				     
				     queryCommd = queryCommd + "(" + queryStr +")";
				 }
				 
				 while (it.hasNext()) 
				 {
						 String tableName = it.next();
						 
						 
					     String queryFromTime = " AND T_UTCTime between  "
						     		+ startTime
						     		+ " and "
						     		+ endTime ;
					     
					    	     
					     String queryStr = "SELECT  T_UTCTime,T_Longitude,T_Latitude,T_Speed,T_Heading,T_AsynFlag "
					    		 	     + "FROM  trajectory."+ tableName
					                     + " where trajectory."+tableName+".T_TargetID = '"
					                     + CarID + "'  "
					                     + queryFromTime
								     	 + " order by T_UTCTime ASC"
								     	 + " limit 10";
					     
					     queryCommd = queryCommd + " union all " + "(" + queryStr +")";
				  					      
			     }
				 
				 queryCommd  = "Select * from (" + queryCommd+ ")a";
				 
				  try {   
					     
					     
						// 执行SQL语句
						PreparedStatement pstmt = conn
								.prepareStatement(queryCommd);
						ResultSet rs = pstmt.executeQuery();
						
						  
					    
					    int tID;  
					    String utcTime, lon, lat,speed,head,asy;  
				
				        //生成GeoJson
			         
				       
				       
				        while (rs.next()) // 生成查询结果
						{
				        	
				        	lon = rs.getString("T_Longitude");  
				        	lat = rs.getString("T_Latitude"); 
				        	
				        	double lon_f = Float.valueOf(lon) / 1000000.0;
				        	double lat_f = Float.valueOf(lat) / 1000000.0;
				        	
				        	if(lon_f < 1.0 || lat_f < 1.0) continue;
				        	
				        	JSONArray point = new JSONArray();
				        	point.put(lon_f);
				        	point.put(lat_f);
				            
				        	coordinatePoints.put(point);  
						}
				       
				        
				        pstmt.close();  // 关闭PreparedStatement对象
				        		        
				        
				   } catch (JSONException e) {  
				        e.printStackTrace();  
				    } catch (Exception e) {  
				        e.printStackTrace();  
				    }  
				 
				
			     JSONObject geoStringerObj = new JSONObject(); 
			     geoStringerObj.put("type", typeFeatrue).put("coordinates", coordinatePoints);
			      
			     geoStringer.object().key("type").value("Feature").key("geometry").value(geoStringerObj).endObject();
				    
				 response.getOutputStream().write(geoStringer.toString().getBytes("UTF-8"));
				 
				 
//				 GPX gpx = new GPX();
//				 Track track = new Track();
//				 track.setTrackPoints(coordinatePoints);
//				 gpx.addTrack(track);
				 
				 
				    
			}
	        catch (Exception e)
			{
				System.out.println(e.getMessage());
			} 
	   
	    
	  
	   
		
		
	}

	/**
	 * The doPost method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to post.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		out.println("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">");
		out.println("<HTML>");
		out.println("  <HEAD><TITLE>A Servlet</TITLE></HEAD>");
		out.println("  <BODY>");
		out.print("    This is ");
		out.print(this.getClass());
		out.println(", using the POST method");
		out.println("  </BODY>");
		out.println("</HTML>");
		out.flush();
		out.close();
	}

	/**
	 * Initialization of the servlet. <br>
	 *
	 * @throws ServletException if an error occurs
	 */
	public void init() throws ServletException {
		// Put your code here
	}
	
	@SuppressWarnings("deprecation")
	public long toGMT(String time) throws ParseException
	{
		Date d = null;
		try {
			d = new SimpleDateFormat("yyyy-MM-dd").parse(time);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(d);
		calendar.setTimeZone(TimeZone.getDefault());
		
		return calendar.getTime().getTime()/1000;
		 
		
	}
	
	
	
	/**
	 *  获取轨迹数据 集合
	 */
	public void getTrajectoryTable()
	{
		
	}
	
	
	/**
	 *   get the valid table in Schema
	 */
	public List<String> getTableSet(Connection conn,String startTime,String endTime)
	{
		List<String> tableSet = new ArrayList<String>();
		
		 // get tableSet
	     
	     String queryTableSet = "select T_TableName as tableName from trajectory.t_logtrackinfo "
	                            + " where T_UTCTimeStart < '"
	    		                + endTime
	    		                +"' AND T_UTCTimeEnd > '"
	    		                + startTime
	    		                +"'";
	     try {   
		     
		     
				// 执行SQL语句
				PreparedStatement pstmt = conn
						.prepareStatement(queryTableSet);
				ResultSet rs = pstmt.executeQuery();
				
			    int tID;  
			    String tableName, lon, lat,speed,head,asy;  
		
		        //生成GeoJson		       
		       
		        while (rs.next()) // 生成查询结果
				{
		        	
		        	tableName = rs.getString("tableName");  	        	
		        	
		        	tableSet.add(tableName);  
				}
		        
		        pstmt.close();  // 关闭PreparedStatement对象
		        		        
		        
		   }catch (Exception e) {  
		        e.printStackTrace();  
		    }  
	     
		
		return tableSet;
	}
	
	/**
	 *   get the Car targetID
	 */
	public String getCarID(Connection conn,String carNumber)
	{
		String carID = "";
		
		 // get tableSet
	     
	     String queryTableSet = "select T_TID from trajectory.t_targetinfo "
                 				+ " where trajectory.t_targetinfo.T_TNumber = '"
                 				+ carNumber + "'";
	     try {   
		     
		     
				// 执行SQL语句
				PreparedStatement pstmt = conn
						.prepareStatement(queryTableSet);
				ResultSet rs = pstmt.executeQuery();
						
		        //生成GeoJson		       
		       
		        while (rs.next()) // 生成查询结果
				{
		        	carID = rs.getString("T_TID");  	        		
				}
		        
		        pstmt.close();  // 关闭PreparedStatement对象
		        		        
		        
		   }catch (Exception e) {  
		        e.printStackTrace();  
		    }  
	     
		
		return carID;
	}
	
	
}
