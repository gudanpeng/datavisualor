package Event;

public class StopEvent {
	
	public int id;
	public String name;
	public double lon;
	public double lat;
	public String time;
	
	public StopEvent(int id, String name, double lon, double lat,String time){
		super();
		this.id =  id;
		this.name =  name;
		this.lon = lon;
		this.lat = lat;
		this.time = time;
		
	}
	

}
