package DataManager;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;
import org.json.JSONWriter;

public class EventServletCsv extends HttpServlet {

	/**
	 * Constructor of the object.
	 */
	public EventServletCsv() {
		super();
	}

	/**
	 * Destruction of the servlet. <br>
	 */
	public void destroy() {
		super.destroy(); // Just puts "destroy" string in log
		// Put your code here
	}

	/**
	 * The doGet method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

			
//	     response.setContentType("text/plain;charset=utf-8");
//	     request.setCharacterEncoding("utf-8");
	     
//	     PrintWriter out = response.getWriter();
//	     String data = "[{name:\"胡阳\",age:24},{name:\"胡阳\",age:23}]";//构建的json数据
//	     out.println(data);
		response.setHeader("Access-Control-Allow-Origin", "*");
		response.setContentType("text/json;charset=UTF-8");
		
		
		//  Type Multipoint MultiLineString
		
		//URL ： ~EventServletCsv? TID= &TOwner= &TNumber= &TStartTime= &TEndTime= &Type=

	     //获取数据库数据
	     try
			{
				javax.naming.Context ctx = new javax.naming.InitialContext();
				
				  // 根据webdb数据源获得DataSource对象
				 javax.sql.DataSource ds = (javax.sql.DataSource) ctx
							.lookup("java:/comp/env/jdbc/trajectory");
				 Connection conn = ds.getConnection();
				
				
							
//				 Map map = request.getParameterMap();  
//			     Set<String> keySet = map.keySet();  
//			     for (String key : keySet) {  
//			        String[] values = (String[]) map.get(key);  
//			        for (String value : values) {  
//			            System.out.println(key+"="+value);  
//			        }
//			     }
				
			     byte dID[] = request.getParameter("TID").getBytes("ISO-8859-1");
			     String tId = new String(dID, "utf-8");
				
			     byte dOwner[] = request.getParameter("TOwner").getBytes("ISO-8859-1");
			     String tOwner = new String(dOwner, "utf-8");
			     
			     byte carNum[] = request.getParameter("TNumber").getBytes("ISO-8859-1");
			     String carNumber = new String(carNum, "utf-8");
			     
			     byte startT[] = request.getParameter("TStartTime").getBytes("ISO-8859-1");
			     String startTime = new String(startT, "utf-8");
			     
			     byte endT[] = request.getParameter("TEndTime").getBytes("ISO-8859-1");
			     String endTime = new String(endT, "utf-8");
			     
			     String typeFeatrue = "Point";
			    
			     
			    // carNumber = "鄂AXW375";
			     
			     if(carNumber==null) return ;
			     if(startTime==null) return ;
			     if(endTime == null) return ; 
			     
			     
			     Calendar calendar = Calendar.getInstance();
			     long startDate = this.toGMT(startTime);
			     long endDate =  this.toGMT(endTime);
			     long interval =  endDate - startDate;
			     long dayNumber = interval /(24*60*60);
			     
			     calendar.setTime(new SimpleDateFormat("yyyy-MM-dd").parse(startTime));
			     			     
			     startTime = String.valueOf(startDate);
			     endTime = String.valueOf(endDate);
			     
			     
				 
				 JSONArray   events = new JSONArray();
			     
			     for(int i = 1; i<=dayNumber; i++)
			     {
			    	 
			    	 
			    	
			    	 
			    	// get DateBaseName
			    	 
			    	 calendar.add(Calendar.DATE, 1);
			    	 
			    	 int day = calendar.get(Calendar.DAY_OF_MONTH);
			    	 int month = calendar.get(Calendar.MONTH) + 1;
			    	 int year = calendar.get(Calendar.YEAR);
			    	 String dataDay ="";
			    	 String dataMonth ="";
			    	 if(day<10) dataDay +="0";
			    	 if(month<10) dataMonth +="0";
				     String dataDate = String.valueOf(year) + dataMonth +  String.valueOf(month) + dataDay  + String.valueOf(day);
				     String tableName = "t_logtrack_" + dataDate;
				     
				  
				     
				     try {  
						     
						
						     
						     // constructor SQL command
						     
						     String queryFromTime = "AND T_UTCTime between  "
							     		+ startTime
							     		+ " and "
							     		+ endTime ;
						     
						     String stopEvent = " AND "+ tableName + ".T_Speed = 0 ";
						     
						    	     
						     String queryStr = " SELECT  T_UTCTime "
						    		 	     + " FROM  "+ tableName
						                     + " where "+ tableName+ ".T_TargetID = (select T_TID from t_targetinfo "
						                     + " where t_targetinfo.T_TNumber = '"
						                     + carNumber + "' ) "
						                     + queryFromTime
						                     + stopEvent					                     
									     	 + " order by T_UTCTime ASC"
									     	 + " limit 1000";
						     
								     
							// 执行SQL语句
						     PreparedStatement pstmt = conn
									.prepareStatement(queryStr);
							ResultSet rs = pstmt.executeQuery();
							
						    String utcTime, lon, lat,speed,head,asy;  
					
					
					        //生成GeoJson
				         				       
					        while (rs.next()) // 生成查询结果
							{
					        	utcTime = rs.getString("T_UTCTime"); 
					        	
					        	JSONObject time = new JSONObject();
					        	
					        	time.put("time",utcTime);			        	
		
					        	events.put(time);  
							}
					    	pstmt.close();  // 关闭PreparedStatement对象
					 
					        		        
					        
					   } catch (JSONException e) {  
					        e.printStackTrace();  
					    } catch (Exception e) {  
					        //e.printStackTrace();
					    	System.out.println(tableName + " is not exist");
					    	
					    	
					    }  
					 
					 
				
				     
				     
			     }
			     
			     
			     JSONObject geoStringerObj = new JSONObject(); 
			     geoStringerObj.put("Name", carNumber).put("Events", events);
				 response.getOutputStream().write(geoStringerObj.toString().getBytes("UTF-8")); 
				 
				 String head = "carNumber,corporation,owner,phone \t\n";
				 
				 
			}
	        
			catch (Exception e)
			{
				System.out.println(e.getMessage());
			} 
	   
	    
	  
	   
		
		
	}

	/**
	 * The doPost method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to post.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		out.println("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">");
		out.println("<HTML>");
		out.println("  <HEAD><TITLE>A Servlet</TITLE></HEAD>");
		out.println("  <BODY>");
		out.print("    This is ");
		out.print(this.getClass());
		out.println(", using the POST method");
		out.println("  </BODY>");
		out.println("</HTML>");
		out.flush();
		out.close();
	}

	/**
	 * Initialization of the servlet. <br>
	 *
	 * @throws ServletException if an error occurs
	 */
	public void init() throws ServletException {
		// Put your code here
	}
	
	@SuppressWarnings("deprecation")
	public long toGMT(String time) throws ParseException
	{
		Date d = null;
		try {
			d = new SimpleDateFormat("yyyy-MM-dd").parse(time);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(d);
		calendar.setTimeZone(TimeZone.getDefault());
		
		return calendar.getTime().getTime()/1000;
		 
		
	}
	
	
	
	/**
	 *  获取轨迹数据 集合
	 */
	public void getTrajectoryTable()
	{
		
	}
	
}
