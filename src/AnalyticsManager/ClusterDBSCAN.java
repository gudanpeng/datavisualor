package AnalyticsManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.mysql.fabric.xmlrpc.base.Array;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.CoordinateList;
import com.vividsolutions.jts.geom.Envelope;
import com.vividsolutions.jts.index.kdtree.KdNode;
import com.vividsolutions.jts.index.kdtree.KdTree;

public class ClusterDBSCAN {
	
	private DBSCAN _dbsDbscan = null;
	private CoordinateList _coords = null;
	public double _esp = 0;
	public int _minPnt = 0;
	
	public ClusterDBSCAN(CoordinateList coords, double esp, int minPnt) {
		_dbsDbscan = new DBSCAN(esp, minPnt);
		_coords = coords;
		_esp = esp;
		_minPnt = minPnt;
	}
	
	public void setCoordinateList(CoordinateList coords) {
		_coords = coords;
	}
	
	public List<Coordinate[]> getDBScanPoints() {
		return _dbsDbscan.dbscanPoints(_coords, _esp, _minPnt);
	}
	
	public List<Coordinate[]> getDBScanPoints( double esp, int minPnt) {
		return _dbsDbscan.dbscanPointsWithoutIndexTree(_coords, esp, minPnt);
	}
	
	public Coordinate[] getDBSCANCentriodPoints() {
		List<Coordinate[]> clusters = _dbsDbscan.dbscanPoints(_coords, _esp, _minPnt);
		
		Coordinate[] centeriodPoints = new Coordinate[ clusters.size()];
		
		int index = 0;
		while(index <  clusters.size()) {
			Coordinate [] cluster = clusters.get(index);
			int currentClusterCount = 0;
			double lon = 0;
			double lat = 0;
			for(Coordinate p:cluster) {
				lon += p.x;
				lat += p.y;
				currentClusterCount++;
			}
			centeriodPoints[index] = new Coordinate(lon / currentClusterCount, lat / currentClusterCount);
			
			index++;
		}
		
		return centeriodPoints;
	}
	
	public Coordinate[] getDBSCANCentriodPoints(double esp, int minPnt) {
		List<Coordinate[]> clusters = _dbsDbscan.dbscanPointsWithoutIndexTree(_coords, esp, minPnt);
		
		Coordinate[] centeriodPoints = new Coordinate[ clusters.size()];
		
		int index = 0;
		while(index <  clusters.size()) {
			Coordinate [] cluster = clusters.get(index);
			int currentClusterCount = 0;
			double lon = 0;
			double lat = 0;
			for(Coordinate p:cluster) {
				lon += p.x;
				lat += p.y;
				currentClusterCount++;
			}
			centeriodPoints[index] = new Coordinate(lon / currentClusterCount, lat / currentClusterCount);
			
			index++;
		}
		
		return centeriodPoints;
	}
	
	/**
	 * DBSCAN 计算后的 集合
	 * @param coords
	 * @param esp
	 * @param minPnt
	 * @return
	 */
	static public  List<Coordinate[]> dbscanPoints(CoordinateList coords, double esp, int minPnt) {
		
		DBSCAN dbscan  = new DBSCAN();
		
		return dbscan.dbscanPoints(coords, esp, minPnt);
	}	
	
	static public  Coordinate[] dbscanCentriodPoints(CoordinateList coordsList,double esp, int minPnt) {
		
		List<Coordinate[]> clusters = dbscanPoints(coordsList, esp, minPnt);
		
		Coordinate[] centeriodPoints = new Coordinate[ clusters.size()];
		
		int index = 0;
		while(index <  clusters.size()) {
			Coordinate [] cluster = clusters.get(index);
			int currentClusterCount = 0;
			double lon = 0;
			double lat = 0;
			for(Coordinate p:cluster) {
				lon += p.x;
				lat += p.y;
				currentClusterCount++;
			}
			centeriodPoints[index] = new Coordinate(lon / currentClusterCount, lat / currentClusterCount);
			
			index++;
		}
		
		return centeriodPoints;
	}
	
}

class DBSCAN {
	public KdTree _indexKdTree = null;
	public double _esp = 0;
	public double _minPnt = 0;
	
	 /** Status of a point during the clustering process. */
    private enum PointStatus {
        /** The point has is considered to be noise. */
        NOISE,
        /** The point is already part of a cluster. */
        PART_OF_CLUSTER
    }

    public DBSCAN(){}
	
	public DBSCAN(double esp, int minPnt){
		_esp = esp;
		_minPnt = minPnt;
	}

	public void createKDTree (Coordinate [] coords) {
		_indexKdTree = new KdTree();
		for (int i = 0; i < coords.length; i++) {
			_indexKdTree.insert(coords[i], i);
		}
	}
	
	public void createKDTree (String [] coords) {
		_indexKdTree = new KdTree();
		
		for (int i = 0; i < coords.length; i++) {
			 
//			indexKdTree.insert(coords[i]);
		}
		
	}
	
	public void createKDTree (Coordinate [] coords, Object [] itemdata) {
		_indexKdTree = new KdTree();
		for (int i = 0; i < coords.length; i++) {
			_indexKdTree.insert(coords[i], itemdata[i]);
		}
	}
	
	/**
	 * DBSCAN 计算后的 集合
	 * @param coords
	 * @param esp
	 * @param minPnt
	 * @return
	 */
	public List<Coordinate[]> dbscanPointsWithoutIndexTree(CoordinateList coords, double esp, int minPnt) {
		//
		Coordinate[] coordsSetCoordinates = coords.toCoordinateArray();
		
		_esp = esp;
		_minPnt = minPnt;
		
		final Map<Coordinate, PointStatus> visited = new HashMap<Coordinate, PointStatus>();
		
		List<Coordinate[]> clusters = new LinkedList<Coordinate[]>(); 
		
		// find Density Points
		for(int indexCoords = 0; indexCoords < coordsSetCoordinates.length; indexCoords++) {
			
			Coordinate point = coordsSetCoordinates[indexCoords];
			
			PointStatus currentPoint= visited.get(point);			
			
			if (currentPoint!=null) continue;
			
			// neighbor
			List<Coordinate> neighborPts = getNeibhborPts(point);
			
			// 判断是否为核心点
			if (neighborPts.size() < minPnt) {
				// 噪声点
				visited.put(point,PointStatus.NOISE);
				
			} else {
				// 为核心点
				clusters.add(expandCluster(point, neighborPts, visited));
			}
			
		}
		
		return clusters;
	}
	
	/**
	 * DBSCAN 计算后的 集合
	 * @param coords
	 * @param esp
	 * @param minPnt
	 * @return
	 */
	public List<Coordinate[]> dbscanPoints(CoordinateList coords, double esp, int minPnt) {
		//
		Coordinate[] coordsSetCoordinates = coords.toCoordinateArray();
		
		_esp = esp;
		_minPnt = minPnt;
		
		final Map<Coordinate, PointStatus> visited = new HashMap<Coordinate, PointStatus>();
		
		createKDTree(coordsSetCoordinates,coordsSetCoordinates);
		
		List<Coordinate[]> clusters = new LinkedList<Coordinate[]>(); 
		
		// find Density Points
		for(int indexCoords = 0; indexCoords < coordsSetCoordinates.length; indexCoords++) {
			
			Coordinate point = coordsSetCoordinates[indexCoords];
			
			PointStatus currentPoint= visited.get(point);			
			
			if (currentPoint!=null) continue;
			
			// neighbor
			List<Coordinate> neighborPts = getNeibhborPts(point);
			
			// 判断是否为核心点
			if (neighborPts.size() < minPnt) {
				// 噪声点
				visited.put(point,PointStatus.NOISE);
				
			} else {
				// 为核心点
				clusters.add(expandCluster(point, neighborPts, visited));
			}
			
		}
		
		return clusters;
	}
	
	public Coordinate[] expandCluster(
			Coordinate point,
			List<Coordinate> neighbors,
			final Map<Coordinate, PointStatus> visited
			){
		
		List<Coordinate> clusterCoordinates = new ArrayList<Coordinate>(); 
		
		clusterCoordinates.add(point);		
		visited.put(point, PointStatus.PART_OF_CLUSTER);
		
		List<Coordinate> seeds = new ArrayList<Coordinate>(neighbors);
        int index = 0;
        while (index < seeds.size()) {
            final Coordinate current = seeds.get(index);
            PointStatus pStatus = visited.get(current);
            // only check non-visited points
            if (pStatus == null) {
                final List<Coordinate> currentNeighbors = getNeibhborPts(current);
                if (currentNeighbors.size() >= _minPnt) {
                    seeds = merge(seeds, currentNeighbors);
                }
            }

            if (pStatus != PointStatus.PART_OF_CLUSTER) {
                visited.put(current, PointStatus.PART_OF_CLUSTER);
                clusterCoordinates.add(current);
            }

            index++;
        }
        Coordinate [] clusterElement = new Coordinate[clusterCoordinates.size()];
        int indexC = 0;
        while(indexC < clusterCoordinates.size()) {
        	clusterElement[indexC] = clusterCoordinates.get(indexC);
        	indexC++;
        }
		return clusterElement;
	}

	// 获取邻居 
	/**
	 * 
	 * @param point
	 * @return
	 */
	public List<Coordinate> getNeibhborPts(Coordinate point) {
		
		double halfEsp = _esp / 2;
		// 查询邻域 点集合
		List<KdNode> nearNodes = _indexKdTree.query( new Envelope(point.x - halfEsp,point.x + halfEsp,point.y - halfEsp,point.y + halfEsp));
		
		List<Coordinate> neighborPts = new ArrayList<Coordinate>();
		
		Iterator<KdNode> iteractList = nearNodes.iterator();
		
		while(iteractList.hasNext()) {
			
			KdNode node = iteractList.next();
			
			Coordinate nearPoint = (Coordinate) node.getData();
			
			if (point.distance(nearPoint) <= _esp) {
				neighborPts.add(nearPoint);
			}
		}
		return neighborPts;
	}
	
    /**
     * Merges two lists together.
     *
     * @param one first list
     * @param two second list
     * @return merged lists
     */
    private List<Coordinate> merge(final List<Coordinate> one, final List<Coordinate> two) {
        final Set<Coordinate> oneSet = new HashSet<Coordinate>(one);
        for (Coordinate item : two) {
            if (!oneSet.contains(item)) {
                one.add(item);
            }
        }
        return one;
    }
}