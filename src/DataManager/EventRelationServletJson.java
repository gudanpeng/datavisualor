package DataManager;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TimeZone;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;
import org.json.JSONWriter;

public class EventRelationServletJson extends HttpServlet {

	/**
	 * Constructor of the object.
	 */
	public EventRelationServletJson() {
		super();
	}

	/**
	 * Destruction of the servlet. <br>
	 */
	public void destroy() {
		super.destroy(); // Just puts "destroy" string in log
		// Put your code here
	}

	/**
	 * The doGet method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

			
//	     response.setContentType("text/plain;charset=utf-8");
//	     request.setCharacterEncoding("utf-8");
	     
//	     PrintWriter out = response.getWriter();
//	     String data = "[{name:\"胡阳\",age:24},{name:\"胡阳\",age:23}]";//构建的json数据
//	     out.println(data);
		response.setHeader("Access-Control-Allow-Origin", "*");
		response.setContentType("text/json;charset=UTF-8");
		
		
		//  Type Multipoint MultiLineString0.
		
		
		//URL ： ~EventServletJson? TID= &TOwner= &TNumber= &TStartTime= &TEndTime= &Type= &DistanceThreshold=&tDistanceThreshold=

	     //获取数据库数据
	     try
			{
//				javax.naming.Context ctx = new javax.naming.InitialContext();
//				
//				  // 根据webdb数据源获得DataSource对象
//				 javax.sql.DataSource ds = (javax.sql.DataSource) ctx
//							.lookup("java:/comp/env/jdbc/trajectoryPost");
//				 Connection conn = ds.getConnection();
	    	 	 DBConn.Init();	
				 Connection conn = DBConn.getConn();
				
				
							
//				 Map map = request.getParameterMap();  
//			     Set<String> keySet = map.keySet();  
//			     for (String key : keySet) {  
//			        String[] values = (String[]) map.get(key);  
//			        for (String value : values) {  
//			            System.out.println(key+"="+value);  
//			        }
//			     }
				
			     byte dID[] = request.getParameter("TID").getBytes("ISO-8859-1");
			     String tId = new String(dID, "utf-8");
				
			     byte dOwner[] = request.getParameter("TOwner").getBytes("ISO-8859-1");
			     String tOwner = new String(dOwner, "utf-8");
			     
			     byte carNum[] = request.getParameter("TNumber").getBytes("ISO-8859-1");
			     String carNumber = new String(carNum, "utf-8");
			     
			     byte startT[] = request.getParameter("TStartTime").getBytes("ISO-8859-1");
			     String startTime = new String(startT, "utf-8");
			     
			     byte endT[] = request.getParameter("TEndTime").getBytes("ISO-8859-1");
			     String endTime = new String(endT, "utf-8");
			     
			     
			     byte dis[] = request.getParameter("DistanceThreshold").getBytes("ISO-8859-1");
			     double distanceThreshold = Double.valueOf(new String(dis));
			     
			     byte tdis[] = request.getParameter("tDistanceThreshold").getBytes("ISO-8859-1");
			     double timeDistanceThreshold = Double.valueOf(new String(tdis));
			     
			     
			    
			     
			    // carNumber = "鄂AXW375";
			     
			     if(startTime==null && startTime==null) return ;
//			     if(startTime==null) return ;
//			     if(startTime==null) return ; 
			     
			     
			     Calendar calendar = Calendar.getInstance();
			     long startDate = this.toGMT(startTime);
			     long endDate =  this.toGMT(endTime);
			     long interval =  endDate - startDate;
			     long dayNumber = interval /(24*60*60);
			     
			     calendar.setTime(new SimpleDateFormat("yyyy-MM-dd").parse(startTime));
			     			     
			     startTime = String.valueOf(startDate);
			     endTime = String.valueOf(endDate);
			     
			     List<String> tableSet = getTableSet(conn,startTime,endTime);
			      
			     List<String> carIDs = getCarIDs(conn,carNumber);
			     
			     String queryFromTime = "AND \"T_UTCTime\" between  "
				     		+ startTime
				     		+ " and "
				     		+ endTime ;
			     
//			     JSONArray events = getCarEvents2(conn, tableSet, queryFromTime, carIDs, carNumber);
//			     
//			     JSONObject relationGraph = getRelationEventsGraph(events, carIDs, distanceThreshold);
//			     
			     JSONObject relationGraph = getRealtionEventsGraphByPost(conn,tableSet,queryFromTime,carIDs,carNumber,distanceThreshold,timeDistanceThreshold);
			     
			     response.getOutputStream().write(relationGraph.toString().getBytes("UTF-8"));  
			     
			     
				DBConn.Recycle(conn);
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
			} 
	   
	    
	  
	   
		
		
	}

	/**
	 * The doPost method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to post.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		out.println("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">");
		out.println("<HTML>");
		out.println("  <HEAD><TITLE>A Servlet</TITLE></HEAD>");
		out.println("  <BODY>");
		out.print("    This is ");
		out.print(this.getClass());
		out.println(", using the POST method");
		out.println("  </BODY>");
		out.println("</HTML>");
		out.flush();
		out.close();
	}

	/**
	 * Initialization of the servlet. <br>
	 *
	 * @throws ServletException if an error occurs
	 */
	public void init() throws ServletException {
		// Put your code here
		
	}
	
	public long toGMT(String time) throws ParseException
	{
		Date d = null;
		try {
			d = new SimpleDateFormat("yyyy-MM-dd").parse(time);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(d);
		calendar.setTimeZone(TimeZone.getDefault());
		
		return calendar.getTime().getTime()/1000;
		 
		
	}
	
	
	
	/**
	 *  获取轨迹数据 集合
	 */
	public void getTrajectoryTable()
	{
		
	}
	

	/**
	 *   get the valid table in Schema
	 */
	public List<String> getTableSet(Connection conn,String startTime,String endTime)
	{
		List<String> tableSet = new ArrayList<String>();
		
		 // get tableSet
	     
	     String queryTableSet = "select \"T_TableName\" as tableName from t_logtrackinfo "
	                            + " where \"T_UTCTimeStart\" < '"
	    		                + endTime
	    		                +"' AND \"T_UTCTimeEnd\" > '"
	    		                + startTime
	    		                +"'";
	     try {   
		     
		     
				// 执行SQL语句
				PreparedStatement pstmt = conn
						.prepareStatement(queryTableSet);
				ResultSet rs = pstmt.executeQuery();
				
			    int tID;  
			    String tableName, lon, lat,speed,head,asy;  
		
		        //生成GeoJson		       
		       
		        while (rs.next()) // 生成查询结果
				{
		        	
		        	tableName = rs.getString("tableName");  	        	
		        	
		        	tableSet.add(tableName);  
				}
		        
		        pstmt.close();  // 关闭PreparedStatement对象
		        		        
		        
		   }catch (Exception e) {  
		        e.printStackTrace();  
		    }  
	     
		
		return tableSet;
	}
	
	
	/**
	 *   get the Car targetID
	 */
	public List<String> getCarIDs(Connection conn, String carNumbers)
	{
		
		
		 // get tableSet
//		if(carNumbers.contains(","))
//			carNumbers = carNumbers.replace(",", "' OR \"T_TNumber\"='");

	    List<String> carIDList = new ArrayList<String>();
	    if(carNumbers.isEmpty()){
			try {   
			     
		    	 String queryTableSet = "select \"T_TID\",\"T_TNumber\" from t_targetinfo limit 100";
		    	 
		    	 PreparedStatement pstmt = conn
							.prepareStatement(queryTableSet);
		    	 
		    
		    	 ResultSet rs = pstmt.executeQuery();
		    		// 执行SQL语句
		    		 
			        //生成GeoJson		       
			        while (rs.next()) // 生成查询结果
					{
			        	String carID = rs.getString("T_TID");
			        	carIDList.add(carID);
					}
			        					
			    pstmt.close();  // 关闭PreparedStatement对象
			        		        
			        
			   }catch (Exception e) {  
			        e.printStackTrace();  
			    }  
			return carIDList;
		 }
	    
	    String [] carNumberArray =  carNumbers.split(",");
	     try {   
		     
	    	 String queryTableSet = "select \"T_TID\",\"T_TNumber\" from t_targetinfo "
      				+ " where \"T_TNumber\" = ?";
	    	 
	    	 PreparedStatement pstmt = conn
						.prepareStatement(queryTableSet);
	    	 
	    	 for(String carN :carNumberArray ){
	    		 pstmt.setString(1, carN);
	    		 ResultSet rs = pstmt.executeQuery();
	    		// 执行SQL语句
	    		 
		        //生成GeoJson		       
		        while (rs.next()) // 生成查询结果
				{
		        	String carID = rs.getString("T_TID");
		        	carIDList.add(carID);
				}
		        
    		 
	 		}
				
		    pstmt.close();  // 关闭PreparedStatement对象
		        		        
		        
		   }catch (Exception e) {  
		        e.printStackTrace();  
		    }  
	     
		
		return carIDList;
	}
	
	/**
	 *  get CarEvents JSONObject
	 * @param conn
	 * @param tableSet
	 * @param queryFromTime
	 * @param carIDs
	 * @param carNumbers
	 * @return
	 * @throws JSONException
	 */
	public JSONArray getCarEvents(Connection conn, List<String> tableSet, String queryFromTime, List<String> carIDs,String carNumbers) throws JSONException{
	
		
		
		 String queryCommd = "";
	
		 Iterator<String> it = tableSet.iterator();
		 // first one 
		 if(it.hasNext())
		 {
			 String tableName = it.next();
			 String stopEvent = " AND "+ tableName + ".T_Speed = 0 ";
			 
			 
			 String queryCarIDs = tableName+ ".T_TargetID = '" + carIDs.get(0);
			 Iterator<String> itCarID = carIDs.iterator();
			 while(itCarID.hasNext())
			 {
				 queryCarIDs += "' AND " + tableName+ ".T_TargetID = '" + itCarID.hasNext();
			 }
		     
		    	     
		     String queryStr = " SELECT  T_TargetID,T_Longitude,T_Latitude,T_UTCTime "
    		 	     + " FROM  "+ tableName
                     + " where "
                     + queryCarIDs 
                     + queryFromTime
                     + stopEvent					                     
			     	 + " order by T_UTCTime ASC"
			     	 + " limit 10";
		     
		     queryCommd = queryCommd + "(" + queryStr +")";
		 }
	     
		 
		 
	     
		 while (it.hasNext()) 
		 {
				 String tableName = it.next();    
				     // constructor SQL command
		    	  String stopEvent = " AND "+ tableName + ".T_Speed = 0 ";     
		    	  
		    	  String queryCarIDs = tableName+ ".T_TargetID = '" + carIDs.get(0);
				 Iterator<String> itCarID = carIDs.iterator();
				 while(itCarID.hasNext())
				 {
					 queryCarIDs += "' AND " + tableName+ ".T_TargetID = '" + itCarID.next();
				 }
				         	     
				  String queryStr = " SELECT  T_TargetID,T_Longitude,T_Latitude,T_UTCTime "
				    		 	     + " FROM  "+ tableName
				                     + " where "
				    		 	     + queryCarIDs
				                     + queryFromTime
				                     + stopEvent					                     
							     	 + " order by T_UTCTime ASC"
							     	 + " limit 10";
				  queryCommd = queryCommd + " union all " + "(" + queryStr +")";
					      
	     }
		 
		 queryCommd  = "Select * from (" + queryCommd+ ")a";
	     
	     JSONArray resultEvents = new JSONArray();
	     
		  try {  				     
			// 执行SQL语句
		     PreparedStatement pstmt = conn
					.prepareStatement(queryCommd);
			ResultSet rs = pstmt.executeQuery();
			
		    String utcTime = "";  
		    String id, lon, lat;
		    
		    String[] carNumberSet = carNumbers.split(",");
		    
		    for(int i=0;i<carNumberSet.length;i++){
		    	JSONObject carObj = new JSONObject();
		    	carObj.put("id", carIDs.get(i));
		    	carObj.put("name", carNumberSet[i]);
		    	carObj.put("events", new JSONArray());
		    	resultEvents.put(carObj);
		    }
		    
		    HashMap<String,Integer> idIndex = new HashMap<String,Integer>();		   
			 Iterator<String> itCarID = carIDs.iterator();
			 int indexCar =0;
			 while(itCarID.hasNext())
			 {
				 idIndex.put(itCarID.next(),indexCar++);
			 }
		    
	
	        //生成GeoJson
         				       
	        while (rs.next()) // 生成查询结果
			{
	        	id = rs.getString("T_TargetID"); 
	        	
	        	lon = rs.getString("T_Longitude");  
	        	lat = rs.getString("T_Latitude"); 
	        	
	        	double lon_f = Float.valueOf(lon) / 1000000.0;
	        	double lat_f = Float.valueOf(lat) / 1000000.0;
	        	
	        	if(lon_f < 1.0 || lat_f < 1.0) continue;
	        	
	        	utcTime = rs.getString("T_UTCTime"); 
	        	
	        	JSONObject event = new JSONObject();
	        	
	        	int index = idIndex.get(id);
	        	JSONArray events = resultEvents.getJSONObject(index).getJSONArray("events");
	        	
	        	event.put("id",id);	
	        	event.put("lon",lon);
	        	event.put("lat",lat);
	        	event.put("time",utcTime);			        	

	        	events.put(event);  
			}
	    	pstmt.close();  // 关闭PreparedStatement对象
	 
	        		        
	        
	   } catch (JSONException e) {  
	        e.printStackTrace();  
	    } catch (Exception e) {  
	        //e.printStackTrace();
	    	
	    	
	    }  
	   
		return resultEvents;
	}
	
	
	
	public JSONArray getCarEvents2(Connection conn, List<String> tableSet, String queryFromTime, List<String> carIDs,String carNumbers) throws JSONException{

		
		JSONArray resultEvents = new JSONArray();
		try {
			
		
			 // 生成 carEvents的json 数组
			 String[] carNumberSet = carNumbers.split(",");   
			 for(int i=0;i<carNumberSet.length;i++){
			    	JSONObject carObj = new JSONObject();
			    	carObj.put("id", carIDs.get(i));
			    	carObj.put("name", carNumberSet[i]);
			    	carObj.put("events", new JSONArray());
			    	resultEvents.put(carObj);
			    }

			 String stopEvent = " AND \"T_Speed\" = 0 "; 
			
			Iterator<String> itTable = tableSet.iterator();
			while(itTable.hasNext()){
				String tableName = itTable.next();
				PreparedStatement stmt2 = conn.prepareStatement("SELECT  \"T_TargetID\",\"T_Longitude\",\"T_Latitude\",from_unixtime(\"T_UTCTime\") as T_UTCTime "
						+ "From "
						+ tableName
						+ " where \"T_TargetID\"= ? "
						+ queryFromTime
						+ " order by \"T_UTCTime\" ASC"
				     	+ " limit 10");
			
				Iterator<String> itCarId = carIDs.iterator();
				int index = 0;  // carsIndex
				while(itCarId.hasNext()){
					String carId = itCarId.next();
					JSONArray events = resultEvents.getJSONObject(index++).getJSONArray("events");
		
						stmt2.setString(1,carId);
						ResultSet rs = stmt2.executeQuery();
						
						
						String utcTime = "";  
					    String id, lon, lat;
						 while (rs.next()) // 生成查询结果
						 {
					        	id = rs.getString("T_TargetID"); 
					        	
					        	lon = rs.getString("T_Longitude");  
					        	lat = rs.getString("T_Latitude"); 
					        	
					        	double lon_f = Float.valueOf(lon) / 1000000.0;
					        	double lat_f = Float.valueOf(lat) / 1000000.0;
					        	
					        	if(lon_f < 1.0 || lat_f < 1.0) continue;
					        	
					        	utcTime = rs.getString("T_UTCTime"); 
					        	
					        	JSONObject event = new JSONObject();		        	
					        	event.put("id",id);	
					        	event.put("lon",lon_f);
					        	event.put("lat",lat_f);
					        	event.put("time",utcTime);			        	
	
					        	events.put(event);  
						}
						
					}
				stmt2.close();
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return resultEvents;	
		
		
		

	}
	
	/**
	 * 获取 多个轨迹事件之间的 空间拓扑关系图
	 * @param events
	 * @param carIDs
	 * @param threholdDis
	 * @return
	 * @throws JSONException
	 */
	public JSONObject getRelationEventsGraph(JSONArray events, List<String> carIDs,double thresholdDis) throws JSONException{
		JSONObject relationGraph = new JSONObject();
		JSONArray nodesObj = new JSONArray();
		JSONArray linksObj = new JSONArray();
		
		// create nodesObj
		int eventObjsLength = events.length();
		for(int i=0,linkStartIndex=0;i<eventObjsLength; i++){
			JSONArray carEvents = events.getJSONObject(i).getJSONArray("events");
			int eventslength = carEvents.length();
			
			for(int j=0;j<eventslength;j++,linkStartIndex++){
				JSONObject event = carEvents.getJSONObject(j);
				JSONObject node = new JSONObject();
				node.put("name", event.get("time"));
				node.put("group",i);
				nodesObj.put(node);
				
				
				//links in every trajectory
				if((j+1)<eventslength){
					JSONObject link = new JSONObject();
					int source = linkStartIndex;
					int target = source + 1;
					int distanceValue = 1;
					link.put("source", source);
					link.put("target", target);
					link.put("value", distanceValue);
					linksObj.put(link);
				}
				
			}
				
		}
		
		// create linkObj
		//links in every trajectory
		for(int i=0,index = 0;i<eventObjsLength;i++){
			JSONArray carEvents = events.getJSONObject(i).getJSONArray("events");
			
			for(int next=i+1 ,nextIndex =index + carEvents.length();next<eventObjsLength;next++){
				JSONArray nextCarEvents = events.getJSONObject(next).getJSONArray("events");
				HashMap<String,Double> linkSet =  distance(carEvents,nextCarEvents,thresholdDis);
				Iterator<Entry<String, Double>> iter = linkSet.entrySet().iterator();
				while (iter.hasNext()) {
				   Entry<String, Double> entry =  	iter.next();
			       String key = entry.getKey();
				   double val = entry.getValue();
				   String[] keys = key.split(",");
				   
				   JSONObject link = new JSONObject();
				   int source = Integer.valueOf(keys[0]) + index;
				   int target = Integer.valueOf(keys[1]) + nextIndex;
				   int distanceValue = (int) (thresholdDis/val);
				   link.put("source", source);
				   link.put("target", target);
				   link.put("value", distanceValue);
				   linksObj.put(link);
				}
				nextIndex += nextCarEvents.length();
			}
			index += carEvents.length();
		}
		
				
		relationGraph.put("nodes", nodesObj);
		relationGraph.put("links", linksObj);
		return relationGraph;
	}
	
	private HashMap<String,Double> distance(JSONArray traj1, JSONArray traj2, double threholdDis) throws JSONException{
		
		HashMap<String,Double> result = new HashMap<String,Double>();
		
		int length1 = traj1.length();
		int length2 = traj2.length();
		for(int i=0; i<length1;i++){
			JSONObject event = traj1.getJSONObject(i);
			double lon1 = event.getDouble("lon");
			double lat1 = event.getDouble("lat");
			for(int j=0;j<length2;j++){
				JSONObject event2 = traj2.getJSONObject(j);
				double lon2 = event2.getDouble("lon");
				double lat2 = event2.getDouble("lat");
				double distance =  LantitudeLongitudeDist(lon1, lat1, lon2,  lat2);
				if(distance < threholdDis)
					result.put(String.format("%d,%d", i,j), distance); 
			}
		}
		
		return result;
	}
	
	private static final  double EARTH_RADIUS = 6378137;//赤道半径(单位m)  
    
    /** 
     * 转化为弧度(rad) 
     * */  
    private static double rad(double d)  
    {  
       return d * Math.PI / 180.0;  
    }  
      
    /** 
     * 基于余弦定理求两经纬度距离 
     * @param lon1 第一点的精度 
     * @param lat1 第一点的纬度 
     * @param lon2 第二点的精度 
     * @param lat3 第二点的纬度 
     * @return 返回的距离，单位km 
     * */  
    public static double LantitudeLongitudeDist(double lon1, double lat1,double lon2, double lat2) {  
        double radLat1 = rad(lat1);  
        double radLat2 = rad(lat2);  
  
        double radLon1 = rad(lon1);  
        double radLon2 = rad(lon2);  
  
        if (radLat1 < 0)  
            radLat1 = Math.PI / 2 + Math.abs(radLat1);// south  
        if (radLat1 > 0)  
            radLat1 = Math.PI / 2 - Math.abs(radLat1);// north  
        if (radLon1 < 0)  
            radLon1 = Math.PI * 2 - Math.abs(radLon1);// west  
        if (radLat2 < 0)  
            radLat2 = Math.PI / 2 + Math.abs(radLat2);// south  
        if (radLat2 > 0)  
            radLat2 = Math.PI / 2 - Math.abs(radLat2);// north  
        if (radLon2 < 0)  
            radLon2 = Math.PI * 2 - Math.abs(radLon2);// west  
        double x1 = EARTH_RADIUS * Math.cos(radLon1) * Math.sin(radLat1);  
        double y1 = EARTH_RADIUS * Math.sin(radLon1) * Math.sin(radLat1);  
        double z1 = EARTH_RADIUS * Math.cos(radLat1);  
  
        double x2 = EARTH_RADIUS * Math.cos(radLon2) * Math.sin(radLat2);  
        double y2 = EARTH_RADIUS * Math.sin(radLon2) * Math.sin(radLat2);  
        double z2 = EARTH_RADIUS * Math.cos(radLat2);  
  
        double d = Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2)+ (z1 - z2) * (z1 - z2));  
        //余弦定理求夹角  
        double theta = Math.acos((EARTH_RADIUS * EARTH_RADIUS + EARTH_RADIUS * EARTH_RADIUS - d * d) / (2 * EARTH_RADIUS * EARTH_RADIUS));  
        double dist = theta * EARTH_RADIUS;  
        return dist;  
    }
    
    
    public JSONObject getRealtionEventsGraphByPost(Connection conn, List<String> tableSet, String queryFromTime, List<String> carIDs,String carNumbers,double thresholdDis,double timeInterval) throws JSONException, SQLException{

   
    	JSONObject relationGraph = new JSONObject();
		JSONArray nodesObj = new JSONArray();
		JSONArray linksObj = new JSONArray();
		
		HashMap<String,Integer> carIdMap = new HashMap<String,Integer> ();
		
		// general carIDStr
		StringBuilder sb = new StringBuilder();
		int indexCarId = 0;
		for (String s : carIDs)
		{
			sb.append(" \"T_TargetID\"='");
		    sb.append(s);
		    sb.append("' OR");
		    carIdMap.put(s, indexCarId);
			indexCarId++;
		    
		}
		String carIDStr = sb.toString().substring(0, sb.length()-2);
		
		int nodeIndex = 0;
		int nextLinkLength = 0;
		Iterator<String> itTable = tableSet.iterator();
		while(itTable.hasNext()){
			String tableName = itTable.next();		
		
			// Create Materialized View
			String stopEvent = " AND \"T_Speed\" = 0 ";     
			
			String createMView = "Drop MATERIALIZED VIEW IF EXISTS public.allEvents;"
								+ " CREATE MATERIALIZED  VIEW public.allEvents AS ("
								+ " SELECT  \"T_TargetID\" as id,\"geom\" as point,\"T_UTCTime\" as utctime "
								+ " From "
								+ tableName
								+ " where "
								+ carIDStr
								+ queryFromTime
								+ stopEvent
								+ " order by random() limit 1000"
								+ ");" ; 
			
			PreparedStatement stmt = conn.prepareStatement(createMView);
			stmt.executeUpdate();
			stmt.close();
			
			HashMap<String,Integer> nodeIndexMap = new HashMap<String,Integer> ();	
			
			
			
			
			String relationSet  = "Select distinct sourceEvents.id as srcid, ST_X(sourceEvents.point) as srcLon,ST_Y(sourceEvents.point) as srcLat ,sourceEvents.utctime as srctime, targetEvents.id as targid,ST_X(targetEvents.point) as targLon,ST_Y(targetEvents.point) as targLat,targetEvents.utctime as targtime"
					              +", ST_Distance(sourceEvents.point::geography,targetEvents.point::geography) as distance"
			                      + " FROM public.allEvents As targetEvents, "
			                      + "( "
//			                      + " SELECT  \"T_TargetID\" as id ,\"geom\" as point,\"T_UTCTime\" as utctime "
								  + " SELECT * "
			                      + " From "
			                      + "public.allEvents" 
//			                      + " where   \"T_TargetID\"=? "
								  + " where   id=? "
//			                      +  queryFromTime
//			                      +  stopEvent
//			                      + " order by utctime ASC limit 100 "
			                      + " ) As sourceEvents"
			                      + " WHERE ST_DWithin(targetEvents.point::geography,sourceEvents.point::geography,"
			                      + thresholdDis
			                      +") AND targetEvents.id<>sourceEvents.id "
			                      + "AND abs(targetEvents.utctime-sourceEvents.utctime) <= "
			                      + timeInterval;


			
			
			
			PreparedStatement stmt2 = conn.prepareStatement(relationSet);
			
			Iterator<String> itCarId = carIDs.iterator();
			while(itCarId.hasNext()){
				String carId = itCarId.next();
				
				stmt2.setString(1,carId);
				ResultSet rs = stmt2.executeQuery();
				
				
						    
			     double distance = 0;
				 while (rs.next()) // 生成查询结果
				 {
					    String id = rs.getString("srcid"); 
					    String targetId = rs.getString("targid");
					    
			        	
			        	double lon_f = Double.valueOf(rs.getString("srcLon"));
			        	double lat_f = Double.valueOf(rs.getString("srcLat"));
			        	
			        	double tlon = Double.valueOf(rs.getString("targLon"));
			        	double tlat = Double.valueOf(rs.getString("targLat"));
			        	
			        	
			        	String utcTime = rs.getString("srctime");
			        	String t_utcTime = rs.getString("targtime");
			        	
			        	distance = Double.valueOf(rs.getString("distance"));
			        	
			        	
			        	
						String srcKey = carId.concat(utcTime);
						String targKey = targetId.concat(t_utcTime);
						
						
						try{
//							int source = nodeIndexMap.get(srcKey);
//							int target =  nodeIndexMap.get(targKey);
							JSONObject link = new JSONObject();
							link.put("srcKey", srcKey);
							link.put("targKey", targKey);
							link.put("value", distance);
							linksObj.put(link);
							
							if(nodeIndexMap.get(srcKey) == null){
								 int group =  carIdMap.get(id);	
								 JSONObject node = new JSONObject();
								 node.put("time",utcTime);
								 node.put("name",group);
								 node.put("group",group);
								 node.put("tid",id);	
								 node.put("lon",lon_f);
								 node.put("lat",lat_f);			        	
								 nodesObj.put(node);
								nodeIndexMap.put(srcKey, nodeIndex++);
							}
							if(nodeIndexMap.get(targKey) == null){
								int group =  carIdMap.get(targetId);	
								 JSONObject node = new JSONObject();
								 node.put("time",t_utcTime);
								 node.put("name",group);
								 node.put("group",group);
								 node.put("tid",targetId);	
								 node.put("lon",tlon);
								 node.put("lat",tlat);			        	
								 nodesObj.put(node);
								nodeIndexMap.put(targKey, nodeIndex++);	
							}
							 
							
						}
						catch (Exception e) {  
					        e.printStackTrace();  
					    }  
						
							
			        	
			        
				}
				
			}
			
			stmt2.close();
			int linkLength =  linksObj.length();
			for(int i=nextLinkLength;i<linkLength;i++){
				JSONObject link = linksObj.getJSONObject(i);
				String srcKey =  (String) link.get("srcKey");
				String targKey =  (String) link.get("targKey");
				int source = nodeIndexMap.get(srcKey);
				int target = nodeIndexMap.get(targKey);
				link.put("source", source);
				link.put("target", target);
				link.remove("srcKey");
				link.remove("targKey");
				
			}
			nextLinkLength = linkLength;
		}
		
		
				
		relationGraph.put("nodes", nodesObj);
		relationGraph.put("links", linksObj);
		return relationGraph;
    }


    protected java.sql.ResultSet execSQL(String sql, Object...args)
    {
    	//
    	return null;
    }

}



